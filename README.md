# Bats: Bash Automated Testing System #

Bats is a lightweight docker container with bats - a TAP testing framework for bash.
It is based on  bash:latest container and uses [bats](https://github.com/sstephenson/bats) github sources for automated build.

# Usage #

docker run darthunix/bats bats <command>
